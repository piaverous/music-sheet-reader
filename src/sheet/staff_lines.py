import cv2
import numpy as np

from utils.rle import rl_encode_2D, rl_decode_2D
from utils.im_utils import remove_black_blocks


def remove_h_lines(img: np.ndarray, line_thickness: int) -> np.ndarray:
    rle, values = rl_encode_2D(img)
    filtered_rle, new_values = [], []
    for i in range(len(rle)):
        res, val = remove_black_blocks(rle[i], values[i], 2 * line_thickness)
        filtered_rle.append(res)
        new_values.append(val)

    return rl_decode_2D(filtered_rle, new_values, img.shape)


def remove_v_lines(img: np.ndarray, line_thickness: int) -> np.ndarray:
    rle, values = rl_encode_2D(img, axis=0)
    filtered_rle, new_values = [], []
    for i in range(len(rle)):
        res, val = remove_black_blocks(rle[i], values[i], 2 * line_thickness)
        filtered_rle.append(res)
        new_values.append(val)

    return rl_decode_2D(filtered_rle, new_values, img.shape, axis=0)


def find_lines_from_edges(edges: np.ndarray, line_spacing: int, im_width=1000) -> list:
    # BIG ASSUMPTION - We assume that staff lines make up for at least half of the page width
    min_line_length = int(im_width / 2)
    # Line detection with Hough transform algorithm
    lines = [(line[0][0], line[0][1]) for line in cv2.HoughLines(edges, 1, np.pi / 180, min_line_length, 500)]

    # Getting cartesian coordinates for lines
    cartesian_lines = []
    for rho, theta in lines:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        x1 = int(x0 + im_width * b)
        y0 = b * rho

        if a < 0.5:  # Remove non horizontal lines
            cartesian_lines.append((y0, x0, x1))
    cartesian_lines = sorted(cartesian_lines, key=lambda l: l[0])

    # Filter out lines that do note respect proper line spacing
    filtered_lines = []
    for index, line in enumerate(cartesian_lines):
        if index > 0 and (line[0] - filtered_lines[-1][0] < 0.9 * line_spacing):
            pass
        else:
            filtered_lines.append(line)

    return filtered_lines
