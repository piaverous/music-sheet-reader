import cv2
import numpy as np
from typing import List

from music.constants import F5_INDEX, ALL_FULL_NOTES
from music.geometry import Rectangle
from music.grid import Grid
from music.notehead import NoteHead
from sheet.staff_lines import find_lines_from_edges


def get_interest_zones(
        edges: np.ndarray,
        line_spacing: int,
        im_width=1000,
        return_lines=False
) -> List[Rectangle] or (List[Rectangle], list):
    filtered_lines = find_lines_from_edges(edges, line_spacing, im_width=im_width)

    top_bottom_lines = []
    for index, line in enumerate(filtered_lines):
        if index > 0 and (line[0] - top_bottom_lines[-1][0] <= 4 * line_spacing):
            pass
        else:
            top_bottom_lines.append(line)

    consec_dists = []
    for i in range(len(top_bottom_lines) - 1):
        consec_dists.append(top_bottom_lines[i + 1][0] - top_bottom_lines[i][0])

    big_spacing = max(consec_dists)
    box_margin = int(big_spacing / 2)

    centers = []
    for i in range(0, len(top_bottom_lines) - 1, 2):
        center_position = int((top_bottom_lines[i + 1][0] - top_bottom_lines[i][0]) / 2)
        center_y = top_bottom_lines[i][0] + center_position
        centers.append((center_y, center_position))

    display_margin = int(big_spacing / 10)
    interest_zones = []
    for (center, offset) in centers:
        top = int(center) - box_margin - offset + display_margin
        bottom = int(center) + box_margin + offset - display_margin
        interest_zones.append(Rectangle(top, bottom, 0, im_width))

    if return_lines:
        return interest_zones, filtered_lines
    return interest_zones


def refine_zones_with_notes(
        note_heads_by_zone: List[List[NoteHead]],
        interest_zones: List[Rectangle],
        line_spacing: int,
        im_width: int = 1000
) -> List[Rectangle]:
    zones_with_notes = []

    for index, zone in enumerate(note_heads_by_zone):
        left_right = np.array([note_head.bounds.left for note_head in zone])

        left = left_right.min() - line_spacing
        top = interest_zones[index].top
        bottom = interest_zones[index].bottom

        zones_with_notes.append(Rectangle(top, bottom, left, im_width))

    return zones_with_notes


def draw_zones_on_image(color_img: np.ndarray, interest_zones: List[Rectangle], color=(255, 0, 0), thickness=2) -> None:
    for zone in interest_zones:
        cv2.rectangle(
            color_img,
            (zone.left, zone.top),
            (zone.right, zone.bottom),
            color,
            thickness
        )


def is_in_any_zone(pixel: (int, int), interest_zones: List[Rectangle]) -> bool:
    return any(map(lambda zone: zone.contains(pixel), interest_zones))


def get_zone_for_pixel(pixel: (int, int), interest_zones: List[Rectangle]) -> int or None:
    for index, zone in enumerate(interest_zones):
        if zone.contains(pixel):
            return index


def get_lines_by_zone(interest_zones: List[Rectangle], identified_lines: list) -> list:
    lines_by_zone = [[] for _ in range(len(interest_zones))]
    for index, zone in enumerate(interest_zones):
        for line in identified_lines:
            if zone.top < line[0] < zone.bottom:
                lines_by_zone[index].append(line[0])
    return lines_by_zone


def get_grid_by_zone(interest_zones: List[Rectangle], identified_lines: list, spacer: int) -> List[Grid]:
    lines_by_zone = get_lines_by_zone(interest_zones, identified_lines)

    for index, zone in enumerate(lines_by_zone):
        lines_above = []
        top_line = zone[0] - spacer
        while top_line > interest_zones[index].top:
            lines_above.append(top_line)
            top_line = top_line - spacer

        lines_below = []
        bottom_line = zone[-1] + spacer
        while bottom_line < interest_zones[index].bottom:
            lines_below.append(bottom_line)
            bottom_line = bottom_line + spacer

        lines_between = [(zone[i] + zone[i + 1]) / 2 for i in range(len(zone) - 1)]

        lines = sorted(np.array(lines_above + zone + lines_below + lines_between, dtype=np.int32))
        notes = [ALL_FULL_NOTES[F5_INDEX + len(lines_above) - i] for i in range(len(lines))]

        lines_by_zone[index] = Grid(lines, notes, interest_zones[index])
    return lines_by_zone
