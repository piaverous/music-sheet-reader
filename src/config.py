NOTE_DURATION = 512
BETWEEN_NOTE_SPACE = 64

REPLAY_FREQ = 44100         # audio CD quality
REPLAY_BITSIZE = -16        # unsigned 16 bit
REPLAY_CHANNELS = 2         # 1 is mono, 2 is stereo
REPLAY_BUFFERSIZE = 1024    # number of samples
