import pygame
from mido import Message, MidiFile, MidiTrack
from typing import List

from config import BETWEEN_NOTE_SPACE, REPLAY_FREQ, REPLAY_BITSIZE, REPLAY_BUFFERSIZE, REPLAY_CHANNELS
from music.notehead import NoteHead


def save_notes_to_midi_files(note_heads_by_zone: List[List[NoteHead]], filename="new_song.mid") -> List[NoteHead]:
    note_succession = [note_head for zone in note_heads_by_zone for note_head in zone]

    mid = MidiFile()
    track = MidiTrack()
    mid.tracks.append(track)
    track.append(Message('program_change', program=12, time=0))

    for note in note_succession:
        track.append(Message('note_on', note=note.height, time=BETWEEN_NOTE_SPACE))
        track.append(Message('note_off', note=note.height, time=note.duration))

    mid.save(filename)
    return note_succession


def play_midi(filename="new_song.mid") -> None:
    pygame.mixer.init(REPLAY_FREQ, REPLAY_BITSIZE, REPLAY_CHANNELS, REPLAY_BUFFERSIZE)

    pygame.mixer.music.load(filename)
    pygame.mixer.music.play()


def stop_midi() -> None:
    pygame.mixer.music.stop()
