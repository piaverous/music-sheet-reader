import numpy as np
from typing import List

from music.notehead import NoteHead


def separate_full_note_heads(note_heads_by_zone: List[List[NoteHead]]) -> List[List[NoteHead]]:
    densities = np.array([note_head.get_pixel_density() for zone in note_heads_by_zone for note_head in zone])

    mean_pixel_density = densities.mean()
    median_pixel_density = (densities.max() + densities.min()) / 2

    print(f"Pixel density | mean: {mean_pixel_density} | median: {median_pixel_density}")

    # Assign note length to corresponding clusters
    for zone in note_heads_by_zone:
        for note_head in zone:
            density = note_head.get_pixel_density()
            note_head.set_rhythm(1 if density < median_pixel_density else 2)

    return note_heads_by_zone


def separate_full_from_halves(note_heads_by_zone: List[List[NoteHead]]) -> List[List[NoteHead]]:
    note_head_lengths = np.array([note_head.bounds.get_h_size() for zone in note_heads_by_zone for note_head in zone])

    mean_note_head_length = note_head_lengths.mean()
    median_note_head_length = (note_head_lengths.max() + note_head_lengths.min()) / 2

    print(f"Note head length | mean: {mean_note_head_length} | median: {median_note_head_length}")

    # Assign note length to corresponding clusters
    for zone in note_heads_by_zone:
        for note_head in zone:
            if note_head.rhythm <= 1:
                note_head.set_rhythm(1 if note_head.bounds.get_h_size() < median_note_head_length else 0.5)

    return note_heads_by_zone
