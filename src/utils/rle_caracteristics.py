import numpy as np
from collections import Counter


def get_consecutive_sum(arr: list) -> list:
    n = len(arr)
    if n == 1:
        return list(arr)
    elif n % 2 == 0:
        return [arr[i] + arr[i + 1] for i in range(0, len(arr) - 1, 2)]
    else:
        result = [arr[i] + arr[i + 1] for i in range(0, len(arr) - 1, 2)]
        result.append(arr[-2] + arr[-1])
        return result


def get_most_common_consec_sum(rle: list) -> int:
    consecutive_sum = [get_consecutive_sum(column) for column in rle]

    flat_structure = []
    for column_data in consecutive_sum:
        flat_structure += column_data

    most_common = np.argmax(np.bincount(flat_structure))
    return most_common


def get_sum_components(arr: list, most_common: int) -> list:
    n = len(arr)
    if n == 1:
        return []
    elif n % 2 == 0:
        return [(arr[i], arr[i + 1]) for i in range(0, len(arr) - 1, 2) if arr[i] + arr[i + 1] == most_common]
    else:
        result = [(arr[i], arr[i + 1]) for i in range(0, len(arr) - 1, 2) if arr[i] + arr[i + 1] == most_common]
        if arr[-2] + arr[-1] == most_common:
            result.append((arr[-2], arr[-1]))
        return result


def get_characteristics(rle: list, m_c=None) -> (int, int, Counter):
    if m_c is None:
        m_c = get_most_common_consec_sum(rle)

    sum_components = [get_sum_components(column, m_c) for column in rle]
    sum_components = [x for x in sum_components if x]

    flat_sum_structure = []
    for column_data in sum_components:
        flat_sum_structure += column_data

    counter = Counter(flat_sum_structure)
    pair, count = counter.most_common()[0]

    line_thickness = min(pair)
    line_spacing = max(pair)

    return line_thickness, line_spacing, counter
