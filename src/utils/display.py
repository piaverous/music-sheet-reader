import matplotlib.pyplot as plt
import numpy as np


def display_img(image: np.ndarray, title='Original image') -> None:
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(25, 40))

    ax.imshow(image, cmap="Greys_r")
    ax.set_title(title)
    ax.axis("off")
