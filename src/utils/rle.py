"""Utils file taken from https://gist.github.com/nvictus/66627b580c13068589957d6ab0919e66

Run Length Encoding utilities for NumPy arrays.
Authors :   
    - Nezar Abdennur
    - Anton Goloborodko
"""
import numpy as np


def rl_encode(x: list, dropna=False) -> (list, list, list):
    """
    Run length encoding.
    Based on http://stackoverflow.com/a/32681075, which is based on the rle 
    function from R.
    
    Parameters
    ----------
    x : 1D array_like
        Input array to encode
    dropna: bool, optional
        Drop all runs of NaNs.
    
    Returns
    -------
    start positions, run lengths, run values
    
    """
    where = np.flatnonzero
    x = np.asarray(x)
    n = len(x)
    if n == 0:
        return (np.array([], dtype=int),
                np.array([], dtype=int),
                np.array([], dtype=x.dtype))

    starts = np.r_[0, where(~np.isclose(x[1:], x[:-1], equal_nan=True)) + 1]
    lengths = np.diff(np.r_[starts, n])
    values = x[starts]

    if dropna:
        mask = ~np.isnan(values)
        starts, lengths, values = starts[mask], lengths[mask], values[mask]

    return starts, lengths, values


def rl_decode(starts: list, lengths: list, values: list, minlength=None) -> list:
    """
    Decode a run-length encoding of a 1D array.
    
    Parameters
    ----------
    starts, lengths, values : 1D array_like
        The run-length encoding.
    minlength : int, optional
        Minimum length of the output array.
    
    Returns
    -------
    1D array. Missing data will be filled with NaNs.
    
    """
    starts, lengths, values = map(np.asarray, (starts, lengths, values))
    # TODO: check validity of rle
    ends = starts + lengths
    n = ends[-1]
    if minlength is not None:
        n = max(minlength, n)
    x = np.full(n, np.nan)
    for lo, hi, val in zip(starts, ends, values):
        x[lo:hi] = val
    return x


def rl_encode_2D(img: np.ndarray, axis=1) -> (list, list):
    # Check we are indeed in 2D
    assert len(img.shape) == 2
    assert abs(axis) <= 1

    if axis == 1:
        rle = [rl_encode(img[:, i])[1] for i in range(img.shape[1])]
        values = [rl_encode(img[:, i])[2] for i in range(img.shape[1])]
        return rle, values
    else:
        rle = [rl_encode(img[i])[1] for i in range(img.shape[0])]
        values = [rl_encode(img[i])[2] for i in range(img.shape[0])]
        return rle, values


def rl_decode_2D(rle: list, values: list, output_shape: (int, int), axis=1) -> np.ndarray:
    # Check we are indeed in 2D
    assert len(output_shape) == 2
    assert abs(axis) <= 1
    assert len(values) == len(rle)

    starts = [[int(np.sum(arr[:i])) for i in range(len(arr))] for arr in rle]

    decoded = np.zeros(output_shape, dtype=np.int32)
    if axis == 1:
        for i in range(decoded.shape[1]):
            decoded[:, i] = rl_decode(starts[i], rle[i], values[i])
    else:
        for i in range(decoded.shape[0]):
            decoded[i] = rl_decode(starts[i], rle[i], values[i])

    return decoded
