import numpy as np
from sklearn.cluster import DBSCAN
from typing import List

from music.cluster import Cluster
from music.geometry import Rectangle
from music.grid import Grid
from music.group import Group
from music.notehead import NoteHead
from sheet.zones import get_zone_for_pixel


def get_note_clusters(pixel_coords: list, line_thickness: int, line_spacing: int, eps: float = None) -> List[Cluster]:
    if eps is None:
        eps = 1.5 * line_thickness
    db = DBSCAN(eps=eps, min_samples=10).fit(pixel_coords)

    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    pixels_by_cluster = [[] for _ in range(n_clusters_)]
    for index, label in enumerate(labels):
        if label == -1:  # Eliminate noise points
            pass
        elif pixels_by_cluster[label]:
            pixels_by_cluster[label].append(pixel_coords[index])
        else:
            pixels_by_cluster[label] = [pixel_coords[index]]

    clusters = [Cluster(pixels) for pixels in pixels_by_cluster]

    # Filter note-sized clusters
    note_filtered = filter_small_clusters(clusters, min_v_size=line_spacing * 0.5,
                                          min_h_size=line_spacing * 0.5)

    # Filter out clusters waaaay too big to be notes
    note_filtered = filter_big_clusters(note_filtered, max_v_size=5 * line_spacing,
                                        max_h_size=5 * line_spacing)

    # Now let's regroup clusters that have areas in common
    grouped_clusters = fuse_intersecting_clusters(note_filtered, tolerance=line_spacing - line_thickness)

    # Filter out clusters too big to be notes
    note_filtered = filter_big_clusters(grouped_clusters, max_v_size=line_spacing + 2 * line_thickness,
                                        max_h_size=2 * line_spacing + 2 * line_thickness)

    # Filter for orientation to be horizontal
    note_filtered = filter_non_horizontal(note_filtered)
    return note_filtered


def get_note_group_clusters(pixel_coords: list, line_spacing: int, eps: float = None) -> List[Group]:
    if eps is None:
        eps = line_spacing
    db = DBSCAN(eps=eps, min_samples=10).fit(pixel_coords)

    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    pixels_by_cluster = [[] for _ in range(n_clusters_)]
    for index, label in enumerate(labels):
        if label == -1:  # Eliminate noise points
            pass
        elif pixels_by_cluster[label]:
            pixels_by_cluster[label].append(pixel_coords[index])
        else:
            pixels_by_cluster[label] = [pixel_coords[index]]

    note_groups = sorted([Group(pixels) for pixels in pixels_by_cluster], key=lambda group: group.bounds.left)

    # Filter out too small groups that are useless
    return [group for group in note_groups if group.bounds.get_h_size() >= line_spacing]


def clusters_to_notes(
        clusters: List[Cluster],
        interest_zones: List[Rectangle],
        grids: List[Grid]
) -> List[List[NoteHead]]:
    # Sort notes by zone
    note_heads_by_zone = [[] for _ in range(len(interest_zones))]
    for cluster in clusters:
        center = cluster.bounds.get_center()
        zone_idx = get_zone_for_pixel(center, interest_zones)
        grid = grids[zone_idx]

        distances = [abs(center[0] - line) for line in grid.h_lines]
        note_height = grid.notes[np.argmin(distances)]

        note_heads_by_zone[zone_idx].append(NoteHead.from_cluster(cluster, note_height))

    # Return notes sorted in read order in each zone
    return [
        sorted(c_zone, key=lambda note_head: note_head.bounds.left) for c_zone in note_heads_by_zone
    ]


def groups_to_notes_by_zone(
        groups: List[Group],
        interest_zones: List[Rectangle]
) -> List[List[NoteHead]]:
    # Sort notes by zone
    note_heads_by_zone = [[] for _ in range(len(interest_zones))]
    for group in groups:
        center = group.bounds.get_center()
        zone_idx = get_zone_for_pixel(center, interest_zones)
        note_heads_by_zone[zone_idx] += group.note_heads

    # Return notes sorted in read order in each zone
    return [
        sorted(c_zone, key=lambda note_head: note_head.bounds.left) for c_zone in note_heads_by_zone
    ]


def filter_big_clusters(clusters: List[Cluster], max_v_size: int, max_h_size: int) -> List[Cluster]:
    return [cluster for cluster in clusters if
            cluster.bounds.get_v_size() <= max_v_size and cluster.bounds.get_h_size() <= max_h_size]


def filter_small_clusters(clusters: List[Cluster], min_v_size: int, min_h_size: int) -> List[Cluster]:
    return [cluster for cluster in clusters if
            cluster.bounds.get_v_size() >= min_v_size and cluster.bounds.get_h_size() >= min_h_size]


def filter_non_horizontal(clusters: List[Cluster]) -> List[Cluster]:
    return [
        cluster for cluster in clusters if cluster.bounds.get_h_size() > cluster.bounds.get_v_size()
    ]


def filter_non_vertical(clusters: List[Cluster]) -> List[Cluster]:
    return [
        cluster for cluster in clusters if cluster.bounds.get_h_size() < cluster.bounds.get_v_size()
    ]


def fuse_intersecting_clusters(clusters: List[Cluster], tolerance: int) -> List[Cluster]:
    grouped_clusters = []
    grouping_clusters = [[x, False] for x in clusters]

    i = 0
    while i < len(grouping_clusters):
        cluster, gr = grouping_clusters[i]
        for j, (cl, g) in enumerate(grouping_clusters):
            if cl == cluster:
                pass
            else:
                if cluster.bounds.intersects(cl.bounds, tolerance=tolerance) and not g:
                    gr = True
                    grouping_clusters[i][1] = True
                    grouping_clusters[j][1] = True
                    grouped_clusters.append(
                        Cluster(np.vstack((cluster.pixels, cl.pixels)))
                    )
        if not gr:
            grouped_clusters.append(cluster)
        i += 1

    return grouped_clusters
