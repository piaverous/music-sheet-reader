import cv2
import numpy as np


def apply_sobel_y(img: np.ndarray, threshold=0.7) -> np.ndarray:
    sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)
    sobely = np.abs(sobely) / (max((np.max(sobely), np.abs(np.min(sobely)))))
    _, thresholded_sobely = cv2.threshold(sobely, threshold, 255, cv2.THRESH_BINARY)

    return thresholded_sobely.astype('uint8')
