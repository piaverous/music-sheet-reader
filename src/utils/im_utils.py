import cv2
import numpy as np
import pandas as pd
from typing import List

from music.geometry import Rectangle


def otsu_bin(img: np.ndarray) -> np.ndarray:
    blur = cv2.GaussianBlur(img, (3, 3), 0)
    ret_val, otsu = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return (otsu / 255).astype(np.int32)


def remove_black_blocks(arr: list, values: list, max_block_height: int):
    assert len(arr) == len(values)  # Make sure this does not explode

    marked_arr = [[arr[i], 'w' if val == 1 else 'b'] for i, val in enumerate(values)]
    for elem in marked_arr:
        if elem[1] == 'b' and elem[0] < max_block_height:
            elem[1] = 'w'

    result, vals = [], []
    next_elem = 0
    for elem in marked_arr:
        if elem[1] == 'w':
            next_elem += elem[0]
        else:
            if next_elem:
                result.append(next_elem)
                vals.append(1)
            result.append(elem[0])
            vals.append(0)
            next_elem = 0
    if next_elem:
        result.append(next_elem)
        vals.append(1)

    return result, vals


def get_black_pixels_in_interest_zones(image: np.ndarray, int_zones: List[Rectangle]) -> np.ndarray:
    arr = np.argwhere(image == 0)
    df = pd.DataFrame(arr)
    result = None
    for zone in int_zones:
        top, bottom, left, right = zone.de_struct()
        if result is None:
            result = df[(df[0] <= bottom) & (df[0] >= top) & (df[1] >= left) & (df[1] <= right)]
        else:
            result = result.append(df[(df[0] <= bottom) & (df[0] >= top) & (df[1] >= left) & (df[1] <= right)])

    return result.to_numpy(dtype=np.int32)
