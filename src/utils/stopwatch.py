import timeit


class StopWatch:
    """Measure time used."""

    def __init__(self):
        self._laps = []

    def start(self) -> None:
        self._start_time = timeit.default_timer()
        self._laps = [self._start_time]
        self._stop_time = None

    def lap(self, msg="Lap") -> None:
        self._laps.append(timeit.default_timer())
        print(f"{msg} - {self.latest_lap_duration()}ms")

    def stop(self, msg="Total") -> None:
        self._stop_time = timeit.default_timer()
        self._laps.append(self._stop_time)
        print(f"{msg} - {self}ms")

    def latest_lap_duration(self) -> str:
        if len(self._laps) >= 2:
            millis = (self._laps[-1] - self._laps[-2]) * 1000
            return f"{millis:.2f}"
        else:
            return "0"

    def __str__(self) -> str:
        millis = (self._stop_time - self._start_time) * 1000
        return f"{millis:.2f}"
