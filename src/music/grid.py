from music.geometry import Rectangle


class Grid:
    def __init__(self, lines: list, notes: list, zone: Rectangle):
        self.h_lines = lines
        self.notes = notes
        self.bounds = zone


