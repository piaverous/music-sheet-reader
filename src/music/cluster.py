import numpy as np
from music.geometry import Rectangle


class Cluster:
    def __init__(self, pixels: list):
        self.pixels = np.array(pixels)
        self.bounds = get_cluster_boundaries(self.pixels)

    def get_pixel_density(self):
        cluster_rect_size = self.bounds.get_v_size() * self.bounds.get_h_size()
        return len(self.pixels) / cluster_rect_size


def get_cluster_boundaries(pixels: np.ndarray) -> Rectangle:
    return Rectangle.from_pixels(pixels)
