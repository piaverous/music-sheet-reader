import numpy as np


class Rectangle:
    def __init__(self, top: int, bottom: int, left: int, right: int):
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right

    def get_v_size(self) -> int:
        return self.bottom - self.top

    def get_h_size(self) -> int:
        return self.right - self.left

    def get_center(self) -> (int, int):
        return int((self.bottom + self.top) / 2), int((self.right + self.left) / 2)

    def intersects(self, other, tolerance=0) -> bool:
        return not (
                self.right + tolerance < other.left
                or self.left - tolerance > other.right
                or self.top - tolerance > other.bottom
                or self.bottom + tolerance < other.top
        )

    def contains(self, pixel: (int, int)) -> bool:
        x, y = pixel
        return self.top <= x <= self.bottom and self.left <= y <= self.right

    def de_struct(self) -> tuple:
        return self.top, self.bottom, self.left, self.right

    def density_for_pixels(self, pixels: list) -> float:
        surface_area = self.get_v_size() * self.get_h_size()
        if surface_area == 0:
            return -1
        return len(pixels) / surface_area

    def __eq__(self, other) -> bool:
        return (
                self.right == other.right
                and self.left == other.left
                and self.top == other.top
                and self.bottom == other.bottom
        )

    def __str__(self):
        return f"Rectangle: TB - {str((self.top, self.bottom)):15} | LR - {str((self.left, self.right)):15}"

    @classmethod
    def from_pixels(cls, pixels: np.ndarray):
        if not isinstance(pixels, np.ndarray):
            pixels = np.array(pixels)
        return cls(min(pixels[:, 0]), max(pixels[:, 0]), min(pixels[:, 1]), max(pixels[:, 1]))
