import numpy as np
from typing import List

from music.cluster import get_cluster_boundaries
from music.geometry import Rectangle
from music.notehead import NoteHead
from sheet.staff_lines import remove_h_lines


class Group:
    def __init__(self, pixels: list, note_heads: List[NoteHead] = None):
        self.pixels = np.array(pixels)

        self.bounds = get_cluster_boundaries(self.pixels)
        self.note_heads = [] if note_heads is None else note_heads

    def contains_note(self, note_head: NoteHead) -> bool:
        # https://stackoverflow.com/a/740384/10494684
        if self.bounds.intersects(note_head.bounds):
            for pixel in note_head.pixels:
                for px in self.pixels:
                    if (pixel == px).all():
                        return True
        return False

    def already_contains_note(self, note_head: NoteHead) -> bool:
        for note in self.note_heads:
            if note == note_head:
                return True
        return False

    def includes_note(self, note_head: NoteHead) -> bool:
        is_in_group = self.contains_note(note_head)
        already_contains = self.already_contains_note(note_head)

        if is_in_group and not already_contains:
            self.note_heads.append(note_head)
        return is_in_group

    def handle_dotted_notes(self, neighbourhood: int = 5) -> int:
        dotted_note_count = 0
        for note_head in self.note_heads:
            right_neighbourhood = Rectangle(
                max(note_head.bounds.top, self.bounds.top),
                min(note_head.bounds.bottom, self.bounds.bottom),
                min(note_head.bounds.right, self.bounds.right),
                min(note_head.bounds.right + neighbourhood, self.bounds.right),
            )
            neighbour_pixels = []
            for px in self.pixels:
                if right_neighbourhood.contains(px):
                    neighbour_pixels.append(px)

            # After defining a neighbourhood to the right of the notehead, we check to see if there are a couple
            # pixels in there. If there are, we assume it is a dot, and that the note is dotted.
            if right_neighbourhood.density_for_pixels(neighbour_pixels) > 0.1:
                note_head.set_rhythm(note_head.rhythm / 1.5)  # Dot next to note multiplies duration by 1.5
                dotted_note_count += 1

        return dotted_note_count

    def is_single_quarter(self, threshold=0.08) -> bool:
        density = self.single_no_head_nor_tail_density()
        is_quarter = density > threshold
        if is_quarter:
            self.note_heads[0].set_rhythm(4)
        return is_quarter

    def single_no_head_nor_tail_density(self) -> float:
        if len(self.note_heads) != 1:
            return -1
        elif self.note_heads[0].rhythm <= 1:
            return -1
        else:
            note_head = self.note_heads[0]
            h_size = self.bounds.get_h_size()
            v_size = self.bounds.get_v_size()
            shape = (v_size + 1, h_size + 1)
            group_as_img = np.ones(shape)

            for px in self.pixels:
                group_as_img[px[0] - self.bounds.top, px[1] - self.bounds.left] = 0

            note_line_image = remove_h_lines(group_as_img, int(v_size / 4))
            result = group_as_img - note_line_image

            if note_head.bounds.get_center()[0] > self.bounds.get_center()[0]:
                result = result[:note_head.bounds.top - self.bounds.top]
            else:
                result = result[note_head.bounds.bottom - self.bounds.top:]

            h, w = result.shape
            return np.count_nonzero(result) / (h * w)