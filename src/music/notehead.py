import numpy as np

from config import NOTE_DURATION, BETWEEN_NOTE_SPACE
from music.cluster import get_cluster_boundaries, Cluster


class NoteHead:
    def __init__(self, pixels: list, height: int, rhythm=2, velocity=60):
        self.pixels = np.array(pixels)
        self.height = height
        self.duration = int(NOTE_DURATION / rhythm)
        self.velocity = velocity
        self.rhythm = rhythm

        self.bounds = get_cluster_boundaries(self.pixels)

    def set_rhythm(self, rhythm: float) -> None:
        self.rhythm = rhythm
        self.duration = int(NOTE_DURATION / rhythm)

    def get_pixel_density(self):
        note_rect_size = self.bounds.get_v_size() * self.bounds.get_h_size()
        return len(self.pixels) / note_rect_size

    def __eq__(self, other):
        return self.bounds == other.bounds

    @classmethod
    def from_cluster(cls, cluster: Cluster, height: int):
        return cls(cluster.pixels, height)
