# Music sheet to MIDI - visual recognition

This project is an attempt at implementing a computer vision algorithm, that can read music sheets and convert them to MIDI.

## Installation

Install dependencies using `pip install -r requirements.txt`.

## Contents

To look at the algorithm in action, you can run the `Cleaned up notebook.ipynb`.

The `src` folder contains all classes and utilitary functions used throughout the project.