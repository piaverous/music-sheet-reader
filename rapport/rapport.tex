\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
\setcounter{page}{1}
\begin{document}

%%%%%%%%% TITLE
\title{Computer Vision Project - Optical Music Recognition}


\author{
Pierre AVEROUS\\
CentraleSupélec\\
{\tt\small pierre.averous@student.ecp.fr}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Jean-Baptiste LAVAL\\
CentraleSupélec\\
{\tt\small jean-baptiste.laval@student.ecp.fr}
}

\maketitle
% \thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
   Optical character recognition (or OCR) is a very well-known technique, for which there exist multiple libraries available to all, such as \texttt{Tesseract} \cite{Tesseract} for instance. This only applies to alpha-numeric characters however. But written text is not the only form of written information transmission. Musical notation is another way of transcribing information into a manuscript, in order to share it with others. Optical Music Recognition (or OMR as it is commonly referred to) techniques are not as popular as those used in OCR, but over the years many have been developped. This paper will present our proceedings, in developing an OMR system able to translate a musical score into a MIDI\footnote{MIDI is an acronym for \textit{Musical Instrument Digital Interface}, a standardized way to describe notes in a numeric format} file.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

In order to share a musical piece with someone, or to teach it to someone, there are two main ways to proceed. Either you have them listen to this musical piece, or you share a written transcript of the musical piece with them. This written transcript is called the musical score.

In this paper we will start with describing a musical score, the rules along which it is built, and the elements that make it up. We will then describe our proceedings in more detail, step by step, following the schema in \textit{Figure \ref{overview_schema}} below.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/omr_process.png}
   \caption{Overview of the process.}
\label{overview_schema}
\end{figure}
%-------------------------------------------------------------------------
\section{The musical score}

As we mentioned in the introduction, a musical score is a written paper, that allows someone to visualize the musical content of a piece. To achieve this, it writes notes on a grid. Notes can be represented in different ways, which will influence the duration they will be played. \textit{Figure \ref{basic_notes}} lists the basic forms of notes, and their duration measured in beats. One beat is a time division whose length can be defined at the top of the score, but its definition is not mandatory. If defined, it is indicated in number of beats per minute (BPM).

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/notes.png}
   \caption{Basic musical notation for notes.}
\label{basic_notes}
\end{figure}

These symbols, give us an idea of the duration a note should be played. These are placed on staff lines, which are equally spaced and give an indication on the height of the note. A "musical line", consists of 5 staff lines and is called a musical stave. The height defines the frequency at which the note will be played. It is subdivided in 7 main notes per octave, $C$, $D$, $E$, $F$, $G$, $A$ and $B$, which correspond to different frequencies. Each staff line and inter-line corresponds to one of these notes, as per \textit{Figure \ref{staff_lines}}. There are however more possibilities than just 7 notes in music, you can play higher or lower even. This is where octaves come in. When starting at the lowest possible $C$, you say you are at octave 0. This $C$ is then written $C_0$. Going up by 8 notes, you arrive at octave 1, so you have $C_1$, $B_1$ and so on.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/staff_lines.png}
   \caption{Staff lines and associated notes.}
\label{staff_lines}
\end{figure}

Also, notes can be altered by a half-tone, by being given accidentals : 
\begin{itemize}
  \item the sharp ($\sharp$) raises the note's pitch by half a tone 
  \item the flat ($\flat$) lowers the note's pitch by half a tone
  \item the natural ($\natural$), which removes any other accidental from the note
\end{itemize}

All these parameters have an impact on how the note should be interpreted and understood. The following table gives a couple examples of how these notes should be interpreted in terms of principal heard frequency, or MIDI code. Following \textit{Table \ref{note_freq_midi}} gives an overview on an example octave.

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|c|r|}
\hline
Pitch & Frequency [Hz] & MIDI code \\
\hline\hline
$C_{-1}$ & 16,35 & 0 \\
$A0$ & 55,00 & 21 \\
$A4$ & 440.00 & 69 \\
$A\sharp4 = B\flat4$ & 466.16 & 70 \\
$B4 = C\flat5$ & 493.88 & 71 \\
$C5 = B\sharp4$ & 523.25 & 72 \\
$C\sharp5 = D\flat5$ & 554.36 & 73 \\
$D5$ & 587.33 & 74 \\
$D\sharp5 = E\flat5$ & 622.25 & 75 \\
$E5 = F\flat5$ & 659.25 & 76 \\
$F5 = E\sharp5$ & 698.46 & 77 \\
$F\sharp5 = G\flat5$ & 739.99 & 78 \\
$G5$ & 783.99 & 79 \\
$G\sharp5 = A\flat5$ & 830.60 & 80 \\
$A5$ & 880.00 & 81 \\
\hline
\end{tabular}
\end{center}
\caption{Note frequencies and MIDI codes.}
\label{note_freq_midi}
\end{table}

In the same manner as how notes describe what to play and when, there exist symbols to describe how and when \textit{not} to play. These are called rests. There is no notion of sound height associated to a rest, as they represent an absence of sound. 

Their duration is coded the same way as for notes, in beat-fractions. The duration equivalents are presented in \textit{Figure \ref{basic_rests}}.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/rests.jpg}
   \caption{Basic musical notation for rests.}
\label{basic_rests}
\end{figure}


%-------------------------------------------------------------------------
\section{Characteristics of the image}

Images of musical scores come in a wide variety of shapes, qualities, and are not easy to process. In order to detect notes on the image, we will need to be able to build descriptors that are robust enough to work when analysing a differently scaled image. To do that, a lot of applications are built around the assumption that the thickness and spacing of staff lines can be found, and used to apply preprocessing to our image. \cite{Rebelo2012} and \cite{BlosteinD.1992} describe many common approaches to do this. The first step is always to binarize the image.

\subsection{Image binarization}

As there is no coloured information in a musical score, it only feels natural to assing a binary balue to every pixel of the image: $0$ for black pixels, $1$ for white ones. This step is critical for images stemming from scanned paper, as there can be nuances of gray, or dark spots that can appear. As per \cite{Burgoyne2007} and \cite{Rebelo2012}, it appears that Otsu's binarization is a standard but valid approach for binarizing musical scores. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/bin_sample.png}
   \caption{Binarized sample of a musical score}
\label{bin_sample}
\end{figure}

We tested ourselves mutiple approaches, such as global or adaptive thresholding, but obtained the best results with Otsu's binarization.


\subsection{Detecting staff lines}

Staff line detection is a very important step of reading a musical score. First of all, the staff lines are the reference component of the score that define the height of every note. Without properly detecting the staff lines, one can never know the exact note that should be played. Also, detecting staff lines efficiently will allow their removal from our digital representation of the sheet, which will make it much easier to detect notes and other symbols.

A common technique (\cite{Rebelo2012}, \cite{Pinto2011}) to detect and remove these staff lines is by encoding the image using the run-length encoding (or RLE) algorithm.

\subsubsection{The RLE algorithm}

This algorithm compresses a sequence of zeros and ones into their successive numbers of occurrences. For example, encoding $11110000$ will result in $[4, 4]$. However, decoding this can be tricky. Does $[4, 4]$ mean there are 4 ones followed by 4 zeroes, or 4 zeroes followed by 4 ones ? A way to proceed, is to use a global convention, that the first number of an RLE sequence always represents the number of ones. Therefore, $00001111$ would be represented as $[0, 4, 4]$ in the RLE space. It is also possible to encode the sequence as a list of pairs, associating the number of occurrences to the corresponding value. $11110000$ would become $[(4,1), (4,0)]$.

Our RLE implementation relies on the second technique, of associating the encoded sequence to the original values.

\subsubsection{RLE for line removal}

To encode the image using the RLE algorithm, there are two options. Either encode it column-wise, or encode it line-wise. The choice will depend on how we plan on detecting lines in the encoded image.

A common technique, as per \cite{Rebelo2012} and \cite{Pinto2011}, is to search for the summed value $line\_thickness+line\_spacing$, instead of looking for both values separately. This can be done by searching the column-wise encoded image for the most common successive sum (black run length + white run length). We then assume that this value corresponds to the aforementioned sum.

Once we know the value of the sum, we can again look at the RLE representation of the image for the most common pair of successive values whose sum are equal to this sum. This pair gives us a good guess of $line\_thickness$ and $line\_spacing$ in the image, by taking the smaller value for thickness, and the bigger one for spacing.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/no_staff_sample.png}
   \caption{Staff lines removed using RLE technique}
\label{no_staff_sample}
\end{figure}

Removing staff lines in the RLE image is then as easy as removing all black runs whose length is smaller than two times the line thickness that we found. 

\subsubsection{Detecting staff lines}

Detecting staff lines can be done using the results of run length encoding. However, we decided to detect lines using Hough transform. We work assuming that lines are perfectly horizontal, and make up at least half of the document's width. Using Hough lines transform after detecting edges proved to work very well on the images in our test base. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/only_staff_sample.png}
   \caption{Image pre-processed for Hough line detection}
\label{only_staff_sample}
\end{figure}

We also found that we could obtain even better results by combining the result of the previously done staff line removal technique with Hough line detection. In fact, if we subtract the image with its lines removed from the original binarized image, we are left with an image containing only the staff lines (see \textit{Figure \ref{only_staff_sample}}). Detecting edges on this image and running it through Hough line detection is very efficient. We obtain results as shown in \textit{Figure \ref{line_detect_sample}}.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/line_detect_sample.png}
   \caption{Lines detected with Hough line detection}
\label{line_detect_sample}
\end{figure}

\subsection{Interest zones}

We use the previously detected line coordinates to define what we call \texttt{Interest zones}, which are the zones of the image that contain musical information. Their height is evaluated using the mean the distance between a musical stave (grouping of 5 staff lines) and the following one. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/interest_zone_sample.png}
   \caption{Interest zones, detected on this music score}
\label{interest_zone_sample}
\end{figure}

Detecting these zones will allow later on to work exclusively inside of these zones, as any information located outside of these zones is not related to musical information. The only exception to that could be the indication of the tempo of the musical piece (in \textit{Figure \ref{interest_zone_sample}}, 140 beats per minute).

\section{Note head detection}

After having obtained as much high level information as we could on our musical score, we now need to see where the note heads are located in the image. Once we find this out, we will be able to evaluate the note height by looking at the note's position between the staff lines. 

A lot of techniques exist to detect notes on the musical score. \cite{CalvinGregory2016} uses template matching to do that, after having scaled the template with the information of line thickness and line spacing. A lot of different techniques have been the subject of research, and many among them are cited in \cite{Rebelo2012}. 

\subsection{Note detection algorithm}

Our approach is to consider note heads as clusters of pixels that have a certain aspect ratio. In order to make recognition of note heads easier, we applied the same technique we used to remove staff lines from the image in order to remove vertical lines as well. We run-length encoded our image line-wise this time, and removed black blocks smaller than twice the estimated staff line thickness (see \textit{Figure \ref{no_v_sample}}). 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/no_v_sample.png}
   \caption{Image after staff and vertical line removal}
\label{no_v_sample}
\end{figure}

Using the \texttt{DBSCAN} clustering algorithm with an epsilon parameter (neighbourhood of a point) of about the thickness of a line, we obtained the results in \textit{Figure \ref{cluster_sample}}. Not only note heads are detected here, but other symbols as well. We can now apply aspect rules to our clusters to keep only those that truly represent note heads.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/cluster_sample.png}
   \caption{Obtained clusters after initial clustering}
\label{cluster_sample}
\end{figure}

\subsection{Note head aspect rules}

The rules we defined have been determined empirically. They all use line spacing and line thickness as reference lengths, in order to have descriptors that do not depend on the image quality. We defined the 3 following rules:

\begin{itemize}
    \item A cluster must be bigger in width and height than a small square of size $line\_spacing/2$
    \item A cluster must be fit into a rectangle of dimensions: 
        \begin{equation}
            \left\{
                \begin{array}{ll}
                    height=line\_spacing + 2*line\_thickness \\
                    width=2 * line\_spacing + 2 * line\_thickness  
                \end{array}
            \right.
        \end{equation}
    \item A cluster must be horizontally oriented, i.e. $width > height$
\end{itemize}

Also, we allowed for clusters whose bounding boxes intersect to fuse together before applying these rules. This was necessary, as half notes were cut into two clusters, as a result of pre-processing. It also helped remove bigger objects, like text or clef symbols.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/cluster_result.png}
   \caption{Obtained clusters after aspect filtering}
\label{cluster_result}
\end{figure}

\section{Note height detection}

Detecting note height consists in assigning a MIDI code to a note head. To do that, we built a grid of horizontal lines in each interest zone, using as a base the 5 staff lines, but adding inter-lines as well. Each line in the grid was mapped to a MIDI code, using the knowledge that the top staff line corresponds to an $F_5$, with midi code $77$ (see \textit{Figure \ref{midi_notes}}). Then, computing the distance between the center of a note head and these lines and mapping the note head to the closest match would give the note height of a note. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/midi_notes.jpg}
   \caption{Notes and associated MIDI codes}
\label{midi_notes}
\end{figure}

%------------------------------------------------------------------------
\section{Detecting rhythm}

\subsection{Differentiating whole and half notes}

Because of the fact that whole and half notes have a different note head than other notes, it only felt natural to start by differentiating them from their counterparts. To do that, we needed no additional information than the one we already had: where are the note heads, and what pixels do they consist of. We approached this with a statistical method, and computed the pixel density inside of the bounding box of a given cluster. We then compared these to the median value. Higher density pixels were classified as "full head" notes, whereas others were "empty head" notes.

To then differentiate whole notes from half notes, we took advantage of the fact that most whole notes have a slightly different aspect ratio than other notes: they are a bit more stretched along their length. Therefore, computing the median length of a note head, those with smaller length were classified as halves, and the remaining ones as whole notes.

\begin{figure}[h]
\frame{\includegraphics[width=\linewidth]{images/whole_half_full.png}}
   \caption{Example of note head classification}
\label{whole_half_full}
\end{figure}

In both of these methods, we used the median value in our statistical distribution. This comes from the fact that the distribution is most of the time very uneven. There are statistically a lot more "full head" notes on a musical score than there are "empty head" notes. The same goes for classification along length: there are a lot more "normal" notes, than whole notes, which are the only note heads with a different aspect ratio. Using the mean value of the statistic distribution proved to lead to many classification errors, whereas the median proved to be a more efficient threshold.

\subsection{Classifying halves and eights}

Now we had to separate different categories of rhythms among full head notes. The note head alone does not convey enough information to be able to differentiate a quarter from an eighth, so we had to take a step back from our "zoomed" vision, that had us focus on a note head, in order to give it more context elements, and look at surrounding pixels.

In order to do that, we determined pixel clusters using the same algorithm as before: \texttt{DBSCAN}. This time however, we did not apply it to the image with no vertical lines, but only to the one with its staff lines removed (see \textit{Figure \ref{no_staff_sample}}). Also, since we were looking to obtain bigger clusters, containing context information about the notes, we used a larger epsilon parameter than before (we set it to the previously evaluated line spacing). Assigning the identified note heads to their respective clusters, we obtained the results shown in \textit{Figure \ref{group_sample}}.

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/group_sample.png}
   \caption{Clustering notes into groups of notes \& context}
\label{group_sample}
\end{figure}

We naively assumed that notes, that were alone in their group were quarters, and that those who were associated to other notes were eighths. This is a big assumption, that can lead to classification errors. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/problem_struct.png}
   \caption{Problematic rhythmic structures for proposed method}
\label{problem_struct}
\end{figure}

This method will not classify sixteenths or lone eighths (\textit{Figure \ref{problem_struct}}) accurately. However, it will allow to classify all quarters accurately. Plus, its results can be fine-tuned. In fact, we developed a method to fix classification mistakes on lone eighths.

\subsection{Fixing classification errors}

The main difference between an eighth and a quarter, is that the eighth has a curved tail, called a flag. In order to determine the presence or absence of such flag, we measure pixel density in the note group that contains the note, after note head and vertical line removal. For quarters, this pixel density is very small as it comes from residual noise. For eighths however, it is quite bigger, as there should be the same amount of noise added to the flag. Statistically measuring the pixel density in all note groups containing a single note, we could separate the eighths from the quarters by estimating that a quarter's pixel density should be in the first quartile of the distribution.

We empirically determined that the first quartile was an appropriate threshold for our problem. The fact that the note head, as we can see in \textit{Figure \ref{problem_struct}}, can be on either side of the note bar whereas the flag is always on the right side, induces a bigger variation in pixel densities among eighths. If the note head is on the left, the surface area for the bounding box of this pixel group will be bigger, resulting in a lower pixel density compared to the case in which the note head is on the right. This phenomenon explains the need for us to take the threshold down from the median value of the distribution, as we would have done instinctively, down to the first quartile.

\section{Enriching a note's context awareness}

Detecting note height and rhythmic structures allow for a great understanding of a musical score. However, context elements of a note such as accidentals, or well simple dots will change the way a note should be interpreted. 

A dot to the right of a note head for example, will extend its duration to $1.5$ times its original duration. A dotted half note will therefore have a duration of $3$ beats, whereas a dotted quarter will last $1.5$ beats.

In order to handle dotted notes, we look at the pixels in a zone in the close right neighbourhood of the note head. We evaluate pixel density inside this zone, and if this is bigger than a certain value, it will mean that the zone most likely contains a dot, and therefore that the note's duration should be multiplied by $1.5$. Except in the case of a dotted note, there rarely are any pixels in the close right neighbourhood of a note head, so we assume these are noise. We empirically found that $0.1$ is an appropriate threshold for this problem (\textit{Figure \ref{dotted_note}}). A pixel density below $10\%$ means that the right neighbourhood of the note head contains either noise or no pixels at all. 

\begin{figure}[h]
\includegraphics[width=\linewidth]{images/dotted_note.png}
   \caption{Dotted note classification example}
\label{dotted_note}
\end{figure}

This approach could also be used to detect dots above or below notes. These mean that the note should be played \textit{staccato}, which means detached from the rest. It should therefore occupy the same number of beats, but the sound should stop sooner than if it was not played with \textit{staccato}.

\section{Future ways to improve}\label{the_future}

\subsection{Image Binarization Technique}

Having a quality binarization algorithm is key in order to have a representation of the musical score that is as close to the reality as possible. \cite{Burgoyne2007} does display some limits to Otsu's binarization in the case of OMR. It would be interesting to look into implementing the work of Pinto et al. In \cite{Pinto2011} they describe their technique, based on the analysis of line thickness and spacing, in which they use these characteristics of the musical score with adaptive thresholding. They call this \textit{Adaptive Content Aware Music Score Binarization}. 

\subsection{Key signature detection}

In this study our focus was the detection of rhythmic structures and of notes. However, key signatures will influence the height of notes in the whole musical sheet, so their detection is definitely an important part of OMR. Being able to detect accidental within the music is also important, as they belong to the basics of music theory.

\subsection{Handle more complex rhythms and rests}

Throughout this study, we built a system able to detect most basic rhythmic structures. However, there are more complex rhythms such as triplets, which our algorithm would not yet be able to detect. \cite{Randriamahefa1993} have used a structural approach to formalize complex rhythmic structures, that would be interesting to implement into our system. Their method would also allow for the detection and identification of accidentals.

Rest detection is also an important feature to develop. The symbols for rests however have less of a geometrical nature than notes do. They are harder to describe by rules based on aspect alone. A different approach would be needed to handle these. Template scaling and matching might be the simplest and most efficient approach for these.

\subsection{Hyperparameter tuning}

Many reference values depend in a way on line thickness and line spacing. They are classified around these metrics. However, the way they depend on these metrics has been evaluated empirically. For example, we defined an aspect rule for note head clusters, by saying that a cluster must be bigger in width and height than a small square of size $line\_spacing/2$. We determined that $line\_spacing*k$ with $k=0.5$ was an appropriate threshold that worked in most cases, but we could have tuned this hyperparameter with more precision by using for example a Grid Search algorithm.

\section{Conclusion}

As discussed in part \ref{the_future}, our algorithm still has a lot to learn. However, we believe it is a robust base for future improvements. We found ways not only to successfully separate note heads from their bodies, but also to enrich the note head with information from its context. The complexity of music notation implies to go further in structure and context detection, which can be handled with computer vision methods similar to the ones we presented here. At some point, we could even imagine hybrid methods based on both a geometrical and a template approaches to reach higher performance and robustness. 

All code and images used for this project can be found at \url{https://gitlab.com/piaverous/music-sheet-reader}. The result of a full score analysis can be viewed in \textit{Figure \ref{final_result}} below.

\begin{figure*}
\begin{center}
\includegraphics[width=\textwidth]{images/final_result.png}
   \caption{Final result of our algorithm on a musical score}
\label{final_result}
\end{center}
\end{figure*}

{\small
\bibliographystyle{ieee_fullname}
\bibliography{refs}
}

\end{document}
